package au.com.messagemedia.test.soccerstats.view;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import au.com.messagemedia.test.soccerstats.MatchEventService;
import au.com.messagemedia.test.soccerstats.MatchTime;
import au.com.messagemedia.test.soccerstats.SoccerStatisticsFactory;
import au.com.messagemedia.test.soccerstats.SoccerStatisticsView;

public class SoccerStatisticsViewImplTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void createStatisticsViewAndValidateOutput() {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		OutputStrategy outputStrategy = new SystemOutputStrategy(new PrintStream(outputStream));
		
		MatchEventService service = SoccerStatisticsFactory.createMatchEventService();
		
		String inputCSV = "createStatisticsViewAndValidateOutput.csv"; // "src/test/resources/createStatisticsViewAndValidateOutput.csv";
		SoccerStatisticsView view = SoccerStatisticsFactory.createSoccerStatisticsView(inputCSV , service, outputStrategy);

		MatchTime matchTime = SoccerStatisticsFactory.createMatchTime(90, 0);
		view.outputMatchStatistics(matchTime );

		String outputStreamAsString = new String(outputStream.toByteArray());
		assertEquals("TimeStamp, Team, Possession, Shot, Score\n45:00, A, 50%, 1, 1\n45:00, B, 50%, 2, 1\n", 
				outputStreamAsString);
	}

}

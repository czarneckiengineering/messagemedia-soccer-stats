package au.com.messagemedia.test.soccerstats.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import au.com.messagemedia.test.soccerstats.MatchEvent;
import au.com.messagemedia.test.soccerstats.MatchEventType;
import au.com.messagemedia.test.soccerstats.MatchTime;
import au.com.messagemedia.test.soccerstats.SoccerStatisticsFactory;

public class MatchEventImplTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void createAKickOffEventAndCheckAttributes() {
		MatchTime matchTime = SoccerStatisticsFactory.createMatchTime(0, 0);
		String team = "A";
		MatchEventType matchEventType = MatchEventType.START;
		
		MatchEvent matchEvent = SoccerStatisticsFactory.createMatchEvent(matchTime, matchEventType, team);
		
		assertNotNull(matchEvent);
		
		assertEquals("A", matchEvent.getTeam());
		assertEquals(0, matchEvent.getMatchTime().getMinutes());
		assertEquals(0, matchEvent.getMatchTime().getSeconds());
		assertEquals(MatchEventType.START, matchEvent.getMatchEventType());
	}

	@Test
	public void createEndOfFirstHafEventAndCheckAttributes() {
		MatchTime matchTime = SoccerStatisticsFactory.createMatchTime(45, 0);
		String team = "A";
		MatchEventType matchEventType = MatchEventType.END;
		
		MatchEvent matchEvent = SoccerStatisticsFactory.createMatchEvent(matchTime, matchEventType, team);
		
		assertNotNull(matchEvent);
		
		assertEquals("A", matchEvent.getTeam());
		assertEquals(45, matchEvent.getMatchTime().getMinutes());
		assertEquals(0, matchEvent.getMatchTime().getSeconds());
		assertEquals(MatchEventType.END, matchEvent.getMatchEventType());
	}

	@Test
	public void createStartOfSecondHalfEventAndCheckAttributes() {
		MatchTime matchTime = SoccerStatisticsFactory.createMatchTime(45, 0);
		String team = "A";
		MatchEventType matchEventType = MatchEventType.START;
		
		MatchEvent matchEvent = SoccerStatisticsFactory.createMatchEvent(matchTime, matchEventType, team);
		
		assertNotNull(matchEvent);
		
		assertEquals("A", matchEvent.getTeam());
		assertEquals(45, matchEvent.getMatchTime().getMinutes());
		assertEquals(0, matchEvent.getMatchTime().getSeconds());
		assertEquals(MatchEventType.START, matchEvent.getMatchEventType());
	}

	@Test
	public void createEndOfMatchEventAndCheckAttributes() {
		MatchTime matchTime = SoccerStatisticsFactory.createMatchTime(90, 0);
		String team = "A";
		MatchEventType matchEventType = MatchEventType.END;
		
		MatchEvent matchEvent = SoccerStatisticsFactory.createMatchEvent(matchTime, matchEventType, team);
		
		assertNotNull(matchEvent);
		
		assertEquals("A", matchEvent.getTeam());
		assertEquals(90, matchEvent.getMatchTime().getMinutes());
		assertEquals(0, matchEvent.getMatchTime().getSeconds());
		assertEquals(MatchEventType.END, matchEvent.getMatchEventType());
	}

}

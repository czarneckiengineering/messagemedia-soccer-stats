package au.com.messagemedia.test.soccerstats.model;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import au.com.messagemedia.test.soccerstats.MatchStatistics;
import au.com.messagemedia.test.soccerstats.MatchTime;
import au.com.messagemedia.test.soccerstats.SoccerStatisticsFactory;

public class MatchStatisticsImplTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void createMatchStatisticsAndCheckAllGetters() {
		MatchTime teamMatchTime = SoccerStatisticsFactory.createMatchTime(2, 10);
		MatchTime totalMatchTime = SoccerStatisticsFactory.createMatchTime(4, 20);
		String team = "A";
		int score = 1;
		int shot = 3;
		
		MatchStatistics matchStatistics = SoccerStatisticsFactory.createMatchStatistics(totalMatchTime, teamMatchTime, team, shot, score);
		
		assertEquals("A", matchStatistics.getTeam());
		assertEquals(1, matchStatistics.getScore());
		assertEquals(3, matchStatistics.getShot());
		assertEquals(50, matchStatistics.getPossession());
		assertEquals(4, matchStatistics.getTotalMatchTime().getMinutes());
		assertEquals(20, matchStatistics.getTotalMatchTime().getSeconds());
		assertEquals(2, matchStatistics.getTeamMatchTime().getMinutes());
		assertEquals(10, matchStatistics.getTeamMatchTime().getSeconds());
	}

}

package au.com.messagemedia.test.soccerstats.model;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import au.com.messagemedia.test.soccerstats.MatchTime;
import au.com.messagemedia.test.soccerstats.SoccerStatisticsFactory;

public class MatchTimeImplTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSetAndGetMinutesAndSeconds() {
		MatchTime matchTime1 = SoccerStatisticsFactory.createMatchTime(2, 10);
		
		assertEquals(2, matchTime1.getMinutes());
		assertEquals(10, matchTime1.getSeconds());

		MatchTime matchTime2 = SoccerStatisticsFactory.createMatchTime(0, 0);
		
		assertEquals(0, matchTime2.getMinutes());
		assertEquals(0, matchTime2.getSeconds());

		MatchTime matchTime3 = SoccerStatisticsFactory.createMatchTime(0, 90);
		
		assertEquals(1, matchTime3.getMinutes());
		assertEquals(30, matchTime3.getSeconds());
	}

}

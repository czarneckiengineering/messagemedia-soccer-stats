package au.com.messagemedia.test.soccerstats;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MatchEventTypeTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testValueOf() {
		assertEquals(MatchEventType.START, MatchEventType.valueOf("START"));
		assertEquals(MatchEventType.END, MatchEventType.valueOf("END"));
		assertEquals(MatchEventType.POSSESS, MatchEventType.valueOf("POSSESS"));
		assertEquals(MatchEventType.SCORE, MatchEventType.valueOf("SCORE"));
		assertEquals(MatchEventType.SHOT, MatchEventType.valueOf("SHOT"));
		assertEquals(MatchEventType.BREAK, MatchEventType.valueOf("BREAK"));
		assertEquals(MatchEventType.END, MatchEventType.valueOf("END"));

	}

}

package au.com.messagemedia.test.soccerstats.service;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import au.com.messagemedia.test.soccerstats.MatchEvent;
import au.com.messagemedia.test.soccerstats.MatchEventService;
import au.com.messagemedia.test.soccerstats.MatchEventType;
import au.com.messagemedia.test.soccerstats.MatchStatistics;
import au.com.messagemedia.test.soccerstats.SoccerStatisticsFactory;
import au.com.messagemedia.test.soccerstats.SoccerStatisticsView;

public class MatchEventServiceImplTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void createAServiceAndReadASimpleMatchCSV() {
		MatchEventService service = SoccerStatisticsFactory.createMatchEventService();
		
		String matchEventsText = 
				"Time, EventType, Team" + "\n"
				+ "00:00,START,A" + "\n"
				+ "45:00,BREAK,A" + "\n"
				+ "45:00,START,B" + "\n"
				+ "90:00,END,B"  + "\n";
		
		InputStream is = new ByteArrayInputStream(matchEventsText.getBytes());
		List<MatchEvent> matchEvents = service.readCSV(is);
		
		assertEquals(4, matchEvents.size());
		
		assertEquals(MatchEventType.START, matchEvents.get(0).getMatchEventType());
		assertEquals(MatchEventType.BREAK, matchEvents.get(1).getMatchEventType());
		assertEquals(MatchEventType.START, matchEvents.get(2).getMatchEventType());
		assertEquals(MatchEventType.END, matchEvents.get(3).getMatchEventType());

		assertEquals(0, matchEvents.get(0).getMatchTime().getMinutes());
		assertEquals(45, matchEvents.get(1).getMatchTime().getMinutes());
		assertEquals(45, matchEvents.get(2).getMatchTime().getMinutes());
		assertEquals(90, matchEvents.get(3).getMatchTime().getMinutes());
	}

	@Test
	public void createMatchEventsAndValidateStatistics() {
		MatchEventService service = SoccerStatisticsFactory.createMatchEventService();
		
		String matchEventsText = 
				"Time, EventType, Team" + "\n"
				+ "00:00,START,A" + "\n"
				+ "10:00,SHOT,A" + "\n"
				+ "10:00,SCORE,A" + "\n"
				+ "45:00,BREAK,A" + "\n"
				+ "45:00,START,B" + "\n"
				+ "55:00,SHOT,B" + "\n"
				+ "55:00,SCORE,B" + "\n"
				+ "60:00,SHOT,B" + "\n"
				+ "90:00,END,B"  + "\n";
		
		InputStream is = new ByteArrayInputStream(matchEventsText.getBytes());
		List<MatchEvent> matchEvents = service.readCSV(is);

		List<MatchStatistics> matchStatisticsList = service.getMatchStatistics(matchEvents, SoccerStatisticsFactory.createMatchTime(90, 0));
		
		assertEquals(2, matchStatisticsList.size());
	}

}

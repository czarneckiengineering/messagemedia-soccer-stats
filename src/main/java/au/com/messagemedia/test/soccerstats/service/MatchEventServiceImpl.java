package au.com.messagemedia.test.soccerstats.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import au.com.messagemedia.test.soccerstats.MatchEvent;
import au.com.messagemedia.test.soccerstats.MatchEventService;
import au.com.messagemedia.test.soccerstats.MatchEventType;
import au.com.messagemedia.test.soccerstats.MatchStatistics;
import au.com.messagemedia.test.soccerstats.MatchTime;
import au.com.messagemedia.test.soccerstats.SoccerStatisticsFactory;

public class MatchEventServiceImpl implements MatchEventService {

	private static final String COMMA = ",";

	public static MatchEventService create() {
		return new MatchEventServiceImpl();
	}

	private static <T> Predicate<T> distinctByTeam(Function<? super T, ?> keyExtractor) {
	    Set<Object> seen = ConcurrentHashMap.newKeySet();
	    return t -> seen.add(keyExtractor.apply(t));
	}

	private static Function<String, MatchEvent> convertLineToMatchEvent = (line) -> {
		String[] p = line.split(COMMA);// a CSV has comma separated lines

		MatchTime matchTime = SoccerStatisticsFactory.createMatchTime(p[0]);
		MatchEventType matchEventType = MatchEventType.valueOf(p[1]);
		String team = p[2];

		MatchEvent event = SoccerStatisticsFactory.createMatchEvent(matchTime, matchEventType, team);
		return event;
	};
	
	private MatchEventServiceImpl() {
	}

	@Override
	public List<MatchEvent> readCSV(InputStream is) {
		List<MatchEvent> events = new ArrayList<MatchEvent>();
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			events = br.lines().skip(1).map(convertLineToMatchEvent).collect(Collectors.toList());
			br.close();
			
		} catch (IOException e) {
			System.out.println("IO failure while reading input match event data");
		}
		return events;
	}

	@Override
	public List<MatchStatistics> getMatchStatistics(List<MatchEvent> events, MatchTime matchTime) {
		List<MatchStatistics> matchStatisticsList = new ArrayList<MatchStatistics>();
		
		// first get distinct 'team' values from events
		
		List<MatchEvent> eventsWithUniqueTeams = events.stream().filter(distinctByTeam(MatchEvent::getTeam)).collect(Collectors.toList());
		
		// create the baseline objects in the matchStatistics return value
		
		for (MatchEvent matchEvent : eventsWithUniqueTeams) {
			long scores = events.stream()
					.filter((e) -> matchEvent.getTeam().equals(e.getTeam()) 
							&& e.getMatchEventType().equals(MatchEventType.SCORE)
							&& e.getMatchTime().asSeconds() <= matchTime.asSeconds())
					.count();
			long shots = events.stream()
					.filter((e) -> matchEvent.getTeam().equals(e.getTeam()) 
							&& e.getMatchEventType().equals(MatchEventType.SHOT)
							&& e.getMatchTime().asSeconds() <= matchTime.asSeconds())
					.count();
			
			MatchStatistics matchStatistics = SoccerStatisticsFactory.createMatchStatistics(
					SoccerStatisticsFactory.createMatchTime(0), 
					SoccerStatisticsFactory.createMatchTime(0), 
					matchEvent.getTeam(), 
					(int) shots, 
					(int) scores);
			
			matchStatisticsList.add(matchStatistics);
		}
		
		// now add the duration values (team time, total time) This ought to be possible by 
		// implementing a custom Collector but I can't derive it. 
		
		// Will code as a simple SUM on the start and end MatchEvents WHERE start.getTeam = theTeam && end.getTeam <> theTeam 

		// iteration for totalMatchtime. Should just be 90 but this way allows for overtime as well as being a validation.
		// PLUS: I never know how you record a goal in overtime on the first half !
		
		for (MatchStatistics matchStatistics : matchStatisticsList) {
			int totalSeconds = 0, teamSeconds = 0;
			MatchEvent startEvent = null;
			for (MatchEvent endEvent : events) {
				if (startEvent != null 
						&& startEvent.getMatchTime().asSeconds() < matchTime.asSeconds()) {
					int startSeconds = startEvent.getMatchTime().asSeconds();
					int endSeconds = endEvent.getMatchTime().asSeconds();
					
					if (endSeconds >= matchTime.asSeconds()) {
						endSeconds = matchTime.asSeconds();
					}
					
					totalSeconds += 
							(endSeconds - startSeconds);
					
					if (startEvent.getTeam().equals(matchStatistics.getTeam())) {
						teamSeconds += 
								(endSeconds - startSeconds);
					}
				}
				
				startEvent = endEvent;
			}
			
			matchStatistics.setTotalMatchTime(SoccerStatisticsFactory.createMatchTime(totalSeconds));
			matchStatistics.setTeamMatchTime(SoccerStatisticsFactory.createMatchTime(teamSeconds));
		}
		
		return matchStatisticsList;
	}

}

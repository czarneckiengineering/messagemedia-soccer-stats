package au.com.messagemedia.test.soccerstats.view;

import au.com.messagemedia.test.soccerstats.MatchStatistics;

public interface OutputStrategy {
	
	void header();
	
	void row(MatchStatistics statisticsLine);

}

package au.com.messagemedia.test.soccerstats;

import au.com.messagemedia.test.soccerstats.model.MatchEventImpl;
import au.com.messagemedia.test.soccerstats.model.MatchStatisticsImpl;
import au.com.messagemedia.test.soccerstats.model.MatchTimeImpl;
import au.com.messagemedia.test.soccerstats.service.MatchEventServiceImpl;
import au.com.messagemedia.test.soccerstats.view.OutputStrategy;
import au.com.messagemedia.test.soccerstats.view.SoccerStatisticsViewImpl;

public class SoccerStatisticsFactory {

	public static SoccerStatisticsView createSoccerStatisticsView(String inputCSV, MatchEventService service, OutputStrategy outputStrategy) {
		return SoccerStatisticsViewImpl.create(inputCSV, service, outputStrategy);
	}

	public static MatchEventService createMatchEventService() {
		return MatchEventServiceImpl.create();
	}

	public static MatchEvent createMatchEvent(MatchTime matchTime, MatchEventType matchEventType, String team) {
		return MatchEventImpl.create( matchTime,  matchEventType,  team);
	}

	public static MatchTime createMatchTime(String minutesAndSeconds) {
		String[] timeArray = minutesAndSeconds.split(":");
		
		int minutes = Integer.parseInt(timeArray[0]);
		int seconds = Integer.parseInt(timeArray[1]);
		
		return createMatchTime(minutes, seconds);
	}
	
	public static MatchTime createMatchTime(int seconds) {
		int minutes = 0;

		return createMatchTime(minutes, seconds);
	}
	
	public static MatchTime createMatchTime(int minutes, int seconds) {
		return MatchTimeImpl.create(minutes, seconds);
	}
	
	public static MatchStatistics createMatchStatistics(MatchTime totalMatchTime, MatchTime teamMatchTime, String team, int shot, int score) {
		return  MatchStatisticsImpl.create(totalMatchTime, teamMatchTime, team, shot, score);
	}

}

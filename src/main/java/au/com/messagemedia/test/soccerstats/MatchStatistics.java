package au.com.messagemedia.test.soccerstats;

public interface MatchStatistics {
	
	void setTotalMatchTime(MatchTime totalMatchTime);
	
	MatchTime getTotalMatchTime();
	
	void setTeamMatchTime(MatchTime teamMatchTime);
	
	MatchTime getTeamMatchTime();
	
	String getTeam();
	
	int getPossession();
	
	int getShot();
	
	int getScore();

}

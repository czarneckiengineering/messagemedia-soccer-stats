package au.com.messagemedia.test.soccerstats.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import au.com.messagemedia.test.soccerstats.MatchEvent;
import au.com.messagemedia.test.soccerstats.MatchEventService;
import au.com.messagemedia.test.soccerstats.MatchStatistics;
import au.com.messagemedia.test.soccerstats.MatchTime;
import au.com.messagemedia.test.soccerstats.SoccerStatisticsView;

public class SoccerStatisticsViewImpl implements SoccerStatisticsView {

	public static SoccerStatisticsView create(String inputCSV, MatchEventService service,
			OutputStrategy outputStrategy) {
		return new SoccerStatisticsViewImpl(inputCSV, service, outputStrategy);
	}

	private String inputCSV;

	private MatchEventService service;

	private OutputStrategy outputStrategy;

	public SoccerStatisticsViewImpl(String inputCSV, MatchEventService service, OutputStrategy outputStrategy) {
		this.inputCSV = inputCSV;
		this.service = service;
		this.outputStrategy = outputStrategy;
	}

	@Override
	public void outputMatchStatistics(MatchTime matchTime) {
		try {
			InputStream is = getClass().getClassLoader().getResourceAsStream(inputCSV);
			
			if (is == null) {
				is = new FileInputStream(new File(inputCSV));
			}

			List<MatchEvent> events = service.readCSV(is);

			List<MatchStatistics> statistics = service.getMatchStatistics(events, matchTime);

			outputStrategy.header();
			statistics.forEach(outputStrategy::row);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

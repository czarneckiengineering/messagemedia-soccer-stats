package au.com.messagemedia.test.soccerstats;

public enum MatchEventType {
	START, 
	POSSESS, 
	SCORE, 
	SHOT,
	BREAK, 
	END;
}

package au.com.messagemedia.test.soccerstats.model;

import au.com.messagemedia.test.soccerstats.MatchEvent;
import au.com.messagemedia.test.soccerstats.MatchEventType;
import au.com.messagemedia.test.soccerstats.MatchTime;

public class MatchEventImpl implements MatchEvent {

	public static MatchEvent create(MatchTime matchTime, MatchEventType matchEventType, String team) {
		return new MatchEventImpl(matchTime, matchEventType, team);
	}

	private MatchTime matchTime;

	private MatchEventType matchEventType;
	
	private String team;
	
	private MatchEventImpl(MatchTime matchTime, MatchEventType matchEventType, String team) {
		super();
		this.matchTime = matchTime;
		this.matchEventType = matchEventType;
		this.team = team;
	}

	@Override
	public MatchEventType getMatchEventType() {
		return matchEventType;
	}

	@Override
	public MatchTime getMatchTime() {
		return matchTime;
	}

	@Override
	public String getTeam() {
		return team;
	}

}

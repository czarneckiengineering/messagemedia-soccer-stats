package au.com.messagemedia.test.soccerstats;

public interface SoccerStatisticsView {
	
	void outputMatchStatistics(MatchTime matchTime);

}

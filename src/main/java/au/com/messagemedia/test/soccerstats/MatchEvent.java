package au.com.messagemedia.test.soccerstats;

public interface MatchEvent {
	
	MatchEventType getMatchEventType();
	
	MatchTime getMatchTime();
	
	String getTeam();

}

package au.com.messagemedia.test.soccerstats;

import java.io.InputStream;
import java.util.List;

public interface MatchEventService {

	List<MatchEvent> readCSV (InputStream is);
	
	List<MatchStatistics> getMatchStatistics(List<MatchEvent> events, MatchTime matchTime);

}

package au.com.messagemedia.test.soccerstats.model;

import au.com.messagemedia.test.soccerstats.MatchTime;

public class MatchTimeImpl implements MatchTime {

	public static MatchTime create(int minutes, int seconds) {
		return new MatchTimeImpl(minutes, seconds);
	}

	private int seconds;
	
	private MatchTimeImpl(int minutes, int seconds) {
		super();
		this.seconds = minutes * 60 + seconds;
	}

	@Override
	public int getMinutes() {
		return seconds / 60;
	}

	@Override
	public int getSeconds() {
		return seconds % 60;
	}

	@Override
	public int asSeconds() {
		return seconds;
	}

	@Override
	public String toString() {
		return String.format("%02d", getMinutes()) + ":" + String.format("%02d", getSeconds());
	}

}

package au.com.messagemedia.test.soccerstats.model;

import au.com.messagemedia.test.soccerstats.MatchStatistics;
import au.com.messagemedia.test.soccerstats.MatchTime;

public class MatchStatisticsImpl implements MatchStatistics {
	
	public static MatchStatistics create(MatchTime totalMatchTime, MatchTime teamMatchTime, String team, int shot, int score) {
		return new MatchStatisticsImpl(totalMatchTime, teamMatchTime, team, shot, score);
	}

	private MatchTime totalMatchTime;
	private MatchTime teamMatchTime;
	private String team;
	private int shot;
	private int score;
	
	private MatchStatisticsImpl(MatchTime totalMatchTime, MatchTime teamMatchTime, String team, int shot, int score) {
		super();
		this.totalMatchTime = totalMatchTime;
		this.teamMatchTime = teamMatchTime;
		this.team = team;
		this.shot = shot;
		this.score = score;
	}

	@Override
	public MatchTime getTotalMatchTime() {
		return totalMatchTime;
	}

	@Override
	public MatchTime getTeamMatchTime() {
		return teamMatchTime;
	}

	@Override
	public String getTeam() {
		return team;
	}

	@Override
	public int getPossession() {
		return (teamMatchTime.asSeconds() * 100) / totalMatchTime.asSeconds();
	}

	@Override
	public int getShot() {
		return shot;
	}

	@Override
	public int getScore() {
		return score;
	}

	@Override
	public void setTotalMatchTime(MatchTime totalMatchTime) {
		this.totalMatchTime = totalMatchTime;
	}

	@Override
	public void setTeamMatchTime(MatchTime teamMatchTime) {
		this.teamMatchTime = teamMatchTime;
	}

}

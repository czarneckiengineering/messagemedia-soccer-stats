package au.com.messagemedia.test.soccerstats;

public interface MatchTime {

	
	int getMinutes();
	
	int getSeconds();

	int asSeconds();

}

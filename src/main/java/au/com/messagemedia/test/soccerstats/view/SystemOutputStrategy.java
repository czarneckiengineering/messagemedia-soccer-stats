package au.com.messagemedia.test.soccerstats.view;

import java.io.PrintStream;

import au.com.messagemedia.test.soccerstats.MatchStatistics;

public class SystemOutputStrategy implements OutputStrategy {
	
	private PrintStream printStream;

	public SystemOutputStrategy(PrintStream printStream) {
		super();
		this.printStream = printStream;
	}

	public SystemOutputStrategy() {
		this(System.out);
	}

	@Override
	public void header() {
		printStream.println("TimeStamp, Team, Possession, Shot, Score");
	}

	@Override
	public void row(MatchStatistics statisticsLine) {
		printStream.println(
				statisticsLine.getTeamMatchTime() + ", "
				+ statisticsLine.getTeam() + ", "
				+ statisticsLine.getPossession() + "%, "
				+ statisticsLine.getShot() + ", "
				+ statisticsLine.getScore());
	}

}
